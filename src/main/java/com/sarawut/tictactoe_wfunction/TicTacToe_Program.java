/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sarawut.tictactoe_wfunction;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author max-s
 */
public class TicTacToe_Program {

    private static final Scanner ip = new Scanner(System.in);
    private static final Random rnd = new Random();
    private static char[][] table = {{'-', '-', '-'},
    {'-', '-', '-'},
    {'-', '-', '-'}};
    private static char turn = randomTurn();

    public static void main(String[] args) {

        //head of program
        showWelcome();
        showTable(table);

        //main program
        int error = 0;
        for (int i = 1; i <= 9; i++) {
            showTurn(turn, error);
            error = inputPosition(ip.next(), ip.next());
            if (error != 0) {
                i--;
            } else {
                showTable(table);
                if (checkWinner(table, turn, i) != '-') {
                    showWinner(checkWinner(table, turn, i));
                    break;
                }
                turn = changeRound(turn);
            }
        }
    }

    private static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    private static void showTable(char[][] tb) {
        for (int i = 0; i < tb[0].length; i++) {
            System.out.print(" " + (i + 1));
        }
        System.out.println("");
        for (int i = 0; i < tb.length; i++) {
            System.out.print(i + 1);
            for (int j = 0; j < tb[i].length; j++) {
                System.out.print(tb[i][j] + " ");
            }
            System.out.println("");
        }
    }

    private static char randomTurn() {
        if ((rnd.nextInt(10) % 2) == 0) {
            return 'O';
        } else {
            return 'X';
        }
    }

    private static void showTurn(char playerTurn, int errorCode) {
        if (errorCode == 0) {
            System.out.println(playerTurn + " turn");
            System.out.println("Please input Row Col: ");
        } else if (errorCode == 1) {
            System.out.println("Input mismatch! Please input again!");
        } else if (errorCode == 2) {
            System.out.println("Your is out of table, Please input Row "
                    + "Col (not above 3 and less than 1)!: ");
        } else if (errorCode == 3) {
            System.out.println("Your Row Col is not empty, "
                    + "Please input again!: ");
        }
    }

    /*
     * Return 0 = no error
     * Return 1 = error input incorrect type
     * Return 2 = position is out of range
     * Return 3 = position is full
     */
    private static int inputPosition(String yPos, String xPos) {
        if (checkInputError(yPos) && checkInputError(xPos)) {
            return checkNumber(Integer.parseInt(yPos) - 1,
                    Integer.parseInt(xPos) - 1);
        } else {
            return 1;
        }
    }

    private static int checkNumber(int yPosI, int xPosI) {
        if (checkRange(yPosI, xPosI)) {
            if (checkPosition(yPosI, xPosI)) {
                table[yPosI][xPosI] = turn;
                return 0;
            } else {
                return 3;
            }
        } else {
            return 2;
        }
    }

    /*
     * Return true is no inputmismatch
     * Return false is error inputmismatch
     */
    private static boolean checkInputError(String posInput) {
        try {
            Integer.parseInt(posInput);
        } catch (NumberFormatException error) {
            return false;
        }
        return true;
    }

    /*
     * Return true when input is not out of range
     * Return false when input is out of range
     */
    private static boolean checkRange(int y, int x) {
        if (((y + 1) > 0) && ((x + 1) > 0)) {
            if ((table.length >= (y + 1)) && (table[0].length >= (x + 1))) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private static boolean checkPosition(int y, int x) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (i == y && j == x) {
                    if (table[i][j] == '-') {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }
        //out of range
        return true;
    }

    private static char changeRound(char turn) {
        if (turn == 'O') {
            return 'X';
        } else {
            return 'O';
        }
    }

    /*
     * Return 'X' = X win
     * Return 'O' = O win
     * Return '0' = no one win
     * Return '-' = game in progress
     */
    private static char checkWinner(char table[][], char turn, int round) {
        char result = checkHoVe(table, turn);
        if (result == '-') {
            result = checkDiagonal(table, turn);
        }
        if (round == 9) {
            return '0';
        }
        return result;
    }

    //hove = Horizon and Vertical
    private static char checkHoVe(char table[][], char turn) {
        for (int i = 0; i < 3; i++) {
            if (table[i][0] == turn && table[i][1] == turn
                    && table[i][2] == turn) {
                return turn;
            } else if (table[0][i] == turn && table[1][i] == turn
                    && table[2][i] == turn) {
                return turn;
            }
        }
        return '-';
    }

    private static char checkDiagonal(char table[][], char turn) {
        if (table[0][0] == turn && table[1][1] == turn && table[2][2] == turn) {
            return turn;
        } else if (table[2][0] == turn && table[1][1] == turn
                && table[0][2] == turn) {
            return turn;
        } else {
            return '-';
        }
    }

    private static void showWinner(char winner) {
        if (winner == '0') {
            System.out.println("No player win....");
        } else {
            System.out.println("Player " + winner + " win....");
        }
        System.out.println("Bye bye ....");
    }
}
